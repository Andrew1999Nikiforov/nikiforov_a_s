# Nikiforov_A_S
Лабораторная работа 1

# Цель

1. Создать docker
2. Запустить докер

# Ход решения

1. Создаем dockerfile
```
FROM ubuntu:latest
ADD script.sh /script.sh
CMD ["./script.sh"]
```

2. Создаем script.sh
```
#!/bin/bash
cat /var/log/dpkg.log
```

3. Создаем docker 
`docker build -t l1 .`

4. Запускаем docker
`docker run -i -t l1`
